# 1KP

### Publications

- Data in this repository is associated with the following manuscript:
    - [One Thousand Plant Transcriptomes Initiative. 2019. One thousand plant transcriptomes and the phylogenomics of green plants. _Nature_ 574: 679–685.](https://www.nature.com/articles/s41586-019-1693-2)


### 1KP_MAPS_aln_tre

The directory contains archives of all gene tree files and alignments for each 1KP MAPS analysis in the 1KP capstone manuscript. The compressed files are named by the corresponding MAPS analysis in the manuscript. The readme in each compressed file contains the taxon identifiers and species names used in each MASP analysis. The -aln folder contains the alignment files for each gene tree analyzed by MAPS, whereas the -tre folder contains the gene tree files.

### 1KP_ksplots

This directory contains all 1KP Ks plots. The ks2 directory contains Ks plots from Ks 0.0001-2. The ks5 directory contains Ks plots from Ks 0.0001-5. The ks2 and ks5 directories can be downloaded as 1KP_ksplots.tar.gz. We also provided 1153 raw output files from the DupPipe pipeline. Each raw output file is a tab de-limited text file containing the node Ks value for each duplication. All raw output files can be downloaded as 1KP_final_ks_files.tar.gz. The 4 letter code is the species code used in the 1KP project (http://www.onekp.com/samples/list.php).

### 1KP_WGD_phylogeny.pdf

This file is a phylogenetic summary of ancient WGDs inferred by the Barker Lab with the 1KP data set. The phylogeny is the ASTRAL tree from analyses of the 1KP data by the phylogenomics team (https://github.com/smirarab/1kp). Yellow stars represent WGDs inferred from Ks plots and synonymous ortholog divergence analyses; Red stars represent WGDs inferred by Ks plots, synonymous ortholog divergence analyses, and MAPS analyses; Blue squares represent significant bursts of gene duplication inferred by MAPS.
